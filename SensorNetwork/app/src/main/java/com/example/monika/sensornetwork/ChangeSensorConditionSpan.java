package com.example.monika.sensornetwork;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.concurrent.ExecutionException;

public class ChangeSensorConditionSpan extends AppCompatActivity {

    EditText value1=null, value2=null;
    String thingProperties, thingId, sensorType, sensor, datastream, thing, datastreamCondition;
    int datastreamId;
    JSONObject sensorObject=null, thingObject=null;
    JSONArray datastreamsArray = null;
    String hint1="none", hint2="none";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_sensor_state);

        thingProperties = getIntent().getExtras().getString("properties");
        thingId = getIntent().getExtras().getString("id");
        sensorType = getIntent().getExtras().getString("type");
        thing = getIntent().getExtras().getString("thing");
        datastream = getIntent().getExtras().getString("datastream");
        sensor = getIntent().getExtras().getString("sensor");
        datastreamCondition = getIntent().getExtras().getString("datastreamCondition");
        datastreamId = Integer.parseInt(getIntent().getExtras().getString("datastreamId"));

        try {
            sensorObject = new JSONObject(sensor);
            datastreamsArray = new JSONArray(datastream);
            thingObject = new JSONObject(thing);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        String[] strArray;

        if (datastreamCondition.contains(",")){
            strArray = datastreamCondition.split(",");
            hint1 = strArray[0].substring(1, strArray[0].length());
            hint2 = strArray[1].substring(0, strArray[1].length() - 1);
        }

        value1 = (EditText) findViewById(R.id.value1);
        value1.setHint(hint1);
        value2 = (EditText) findViewById(R.id.value2);
        value2.setHint(hint2);

    }

    public void save (View view) throws JSONException {

        String successResponse;
        JSONObject thing = null;
        PatchExample server = null;
        String val1, val2, msg="";
        int numVal1=0, numVal2=0;

        //priprema conditiona za slanje

        val1 = value1.getText().toString();
        val2 = value2.getText().toString();

        try{
            numVal1 = Integer.parseInt(val1);
            numVal2 = Integer.parseInt(val2);
        }
        catch (NumberFormatException e) {
            CharSequence text = "Condition is not valid!";
            int duration = Toast.LENGTH_SHORT;

            Toast toast = Toast.makeText(this, text, duration);
            toast.show();
            e.printStackTrace();
        }

        if (numVal1 < numVal2){
            msg = "{'type':'change conditions', 'datastreamId':" + datastreamId + ", 'condition':[" + numVal1 + ", " + numVal2 + "]}";

            Log.d("UDP", "Pripremam podatke za slanje na UDP server...");
            Log.d("UDP", "Podaci za slanje na UDP " + msg);
            ClientUdpSender cilentSender = new ClientUdpSender(msg);
            try {
                String response = cilentSender.execute().get();
                JSONObject jsonObj = new JSONObject(response);
                Log.d("UDP-response", jsonObj.toString());

                if (jsonObj.has("success")) {

                    successResponse = jsonObj.getString("success");
                    //ako je success true salji na sensorUp izmjene
                    if (successResponse.equals("True")) {

                        Log.d("UDP", "Podaci spremljeni na UDP server");
                        Log.d("UDP", "Pripremam podatke za SensorUp server...");
                        JSONObject sendProperties = prepareData(thingProperties, sensorType, "condition", numVal1, numVal2);
                        try {
                            thing = new JSONObject();
                            thing.put("properties", sendProperties);
                            server = new PatchExample(this, thingId, thing, "displaySensor", sensorObject, datastreamsArray);
                            server.execute();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }else{

                        Log.d("UDP_S", "Success key is set to false");
                        CharSequence text = "Data update failed on server UDP";
                        int duration = Toast.LENGTH_SHORT;
                        Toast toast = Toast.makeText(this, text, duration);
                        toast.show();

                    }
                }else{

                    Log.d("UDP", "No success key in response");
                    CharSequence text = "Data update failed on server UDP";
                    int duration = Toast.LENGTH_SHORT;
                    Toast toast = Toast.makeText(this, text, duration);
                    toast.show();
                }

            } catch (InterruptedException | JSONException e) {
                Log.d("UDP", "InterruptedException");
                e.printStackTrace();
            } catch (ExecutionException e) {
                Log.d("UDP", "ExecutionException");
                e.printStackTrace();
            }
        }
        else{
            value1.setHint(hint1);
            value2.setHint(hint2);
            CharSequence text = "Condition is not valid!";
            int duration = Toast.LENGTH_SHORT;

            Toast toast = Toast.makeText(this, text, duration);
            toast.show();
        }

    }

    private JSONObject prepareData(String thingProperties, String type, String sensorKey, int conditionValue1, int conditionValue2) throws JSONException {

        JSONObject jsonObj = new JSONObject(thingProperties);
        JSONObject conditionObj = jsonObj.getJSONObject(type);
        JSONObject datastreamObj = conditionObj.getJSONObject(sensorKey);
        datastreamObj.remove(String.valueOf(datastreamId));
        datastreamObj.put(String.valueOf(datastreamId), "["+conditionValue1+", "+conditionValue2+"]");

        return jsonObj;
    }

}
