package com.example.monika.sensornetwork;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class DisplayDatastreamsForOnOff extends AppCompatActivity {

    JSONObject thing, properties, configuration, sensorState, sensor;
    JSONArray datastreams;
    String sensorType="", thingId="", sensorName="";
    String values[], doubleKeys[], datastreamIds[];
    List<Map<String, String>> listOfsensors = new ArrayList<Map<String, String>>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display_datastreams_for_on_off);
        ListView lv = (ListView) findViewById(R.id.datastreamListOnOff);

        Bundle bundle=getIntent().getExtras();
        if(bundle!=null) {
            try {

                thing = new JSONObject(bundle.getString("thing"));
                thingId = thing.getString("@iot.id");
                sensor = new JSONObject(bundle.getString("sensor"));
                sensorName = sensor.getString("name");
                datastreams = new JSONArray(bundle.getString("datastreams"));
                sensorType = bundle.getString("sensorType");

                properties = thing.getJSONObject("properties");
                configuration = properties.getJSONObject(sensorType);
                sensorState = configuration.getJSONObject("sensorOn");


            } catch (JSONException e) {
                e.printStackTrace();
            }
        }



        setLayout();

        SimpleAdapter adapter = new SimpleAdapter(this, listOfsensors,
                android.R.layout.simple_list_item_2,
                new String[] {"Sensor", "Datastream"},
                new int[] {android.R.id.text1,
                        android.R.id.text2});

        lv.setAdapter(adapter);

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Intent intent = new Intent(getApplicationContext(), TurnSensorOnOff.class);
                try {
                    intent.putExtra("sensorState", sensorState.getString(doubleKeys[position]));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                intent.putExtra("datastreamId", datastreamIds[position]);
                intent.putExtra("sensorType", sensorType);
                intent.putExtra("properties", properties.toString());
                intent.putExtra("sensor", sensor.toString());
                intent.putExtra("thingId", thingId);
                intent.putExtra("datastreamArray", datastreams.toString());
                intent.putExtra("wholeKey", doubleKeys[position]);
                startActivity(intent);


            }
        });

    }



    private void setLayout(){

        int lenth = sensorState.length();
        values = new String[lenth];
        int l=0;
        String[] wholeKeys = new String[lenth];
        String[] firstKey = new String[lenth];
        String[] sencondKey = new String[lenth];

        int i=0;
        String key="";

        doubleKeys = new String[lenth];
        datastreamIds = new String[lenth];
        Iterator<String> keysIterator = sensorState.keys();

        while (keysIterator.hasNext())
        {
            key = (String)keysIterator.next();
            wholeKeys[i] = key;
            String tmp[] = new String[2];
            tmp = key.split("-");
            firstKey[i] = tmp[0];
            sencondKey[i] = tmp[1];
            try {
                values[i] = sensorState.getString(key);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            i++;
        }

        JSONObject idNameMapp = new JSONObject();
        //ako je vise datastremova od conditiona za on/off
        for(int j = 0; j < datastreams.length(); j++)
        {

                try {

                    String jsonString = datastreams.getString(j);
                    JSONObject tempObject = new JSONObject(jsonString);

                    idNameMapp.put(tempObject.getString("@iot.id"), tempObject.getString("name"));

                } catch (JSONException e) {
                    e.printStackTrace();
                }

        }

        for (int m=0; m < wholeKeys.length; m++){

            Map<String, String> data = new HashMap<String, String>(2);
            try {
                data.put("Sensor", idNameMapp.getString(firstKey[m]) + "-" + idNameMapp.getString(sencondKey[m]));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            data.put("Datastream", sensorName);
            listOfsensors.add(data);

            //svejedno hocemo li uzeti prvi ili drugi key
            datastreamIds[m] = firstKey[m];
            doubleKeys[m] = wholeKeys[m];

        }

    }
}


