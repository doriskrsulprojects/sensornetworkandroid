package com.example.monika.sensornetwork;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Locale;

/**
 * Created by Monika on 26.5.2017..
 */

public class Location1Info extends Fragment implements OnMapReadyCallback{

    private GoogleMap mMap;
    private LatLng start;
    private String title;
    public Location1Info() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.location1info, container, false);

        Bundle bundle=getArguments();
        if(bundle!=null) {

            if (bundle.containsKey("location")) {
                try {
                    JSONObject locationObject = new JSONObject(bundle.getString("location"));

                    TextView iotNameValue = (TextView) rootView.findViewById(R.id.textLocationNameV);
                    TextView iotDescriptionValue = (TextView) rootView.findViewById(R.id.textLocationDescriptionV);
                    TextView x = (TextView) rootView.findViewById(R.id.textLocationCoordinatesXV);
                    TextView y = (TextView) rootView.findViewById(R.id.textLocationCoordinatesYV);

                    iotNameValue.setText(locationObject.getString("name"));
                    iotDescriptionValue.setText(locationObject.getString("description"));

                    JSONObject loc = locationObject.getJSONObject("location");
                    JSONArray coor = loc.getJSONArray("coordinates");

                    double xVal = coor.getDouble(0);
                    double yVal = coor.getDouble(1);

                    x.setText(String.format (Locale.US, "%.4f", xVal));
                    y.setText(String.format (Locale.US, "%.4f", yVal));

                    title = locationObject.getString("name");
                    start = new LatLng(xVal, yVal);


                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }

        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.location_info_map);
        mapFragment.getMapAsync(this);

        return rootView;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        //LatLng UCA = new LatLng(-34, 151);
        mMap.addMarker(new MarkerOptions().position(start).title(title)).showInfoWindow();

        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(start,17));
    }
}
