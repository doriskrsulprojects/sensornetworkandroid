package com.example.monika.sensornetwork;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.SimpleAdapter;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import java.text.SimpleDateFormat;
import static com.example.monika.sensornetwork.R.id.buttonAddSensor;
import static com.example.monika.sensornetwork.R.id.textDatastreamIotNameNewChange;
import static com.example.monika.sensornetwork.R.id.textdatastreamIotDescriptionNewChange;

class GetExample extends AsyncTask<String, Void, String> {
    private ListView listView = null;
    private String buttonPressed = "", getLink ="";
    private String symbol = "";
    private String link = "", observationSymbol;
    private String returnValue = "", entity="", entityId="";
    private Activity activity;
    private final OkHttpClient client = new OkHttpClient();
    private JSONObject JObject;
    private JSONArray things = null;
    private ArrayList<String> lst=null;
    private JSONArray sensors;
    private JSONArray datastreams;
    private JSONArray observations;
    private ArrayList<JSONObject> redudantList;
    private JSONArray datastreamsArray;
    private ArrayList<JSONObject> sensorList, thingList;
    private ArrayList<JSONArray> datastreamList;
    private JSONObject locations;
    private JSONArray locationsList;
    private ProgressBar bar;
    private JSONObject datastreamListThing;


    GetExample(Activity activity, String buttonPressed){
        this.activity = activity; this.buttonPressed = buttonPressed;
    }

    GetExample(Activity activity, String buttonPressed, ProgressBar pb){
        this.activity = activity; this.buttonPressed = buttonPressed; this.bar = pb;
    }

    GetExample(ListView listView, Activity activity, String buttonPressed){
        this.listView = listView; this.activity = activity; this.buttonPressed = buttonPressed;
    }

    GetExample(ListView listView, Activity activity, String buttonPressed, ProgressBar pb){
        this.listView = listView; this.activity = activity; this.buttonPressed = buttonPressed; this.bar = pb;
    }

    GetExample(ListView listView, Activity activity, String buttonPressed, String link){
        this.listView = listView; this.activity = activity; ;this.buttonPressed = buttonPressed; this.link = link;
    }

    GetExample(ListView listView, Activity activity, String buttonPressed, String link, ProgressBar pb){
        this.listView = listView; this.activity = activity; ;this.buttonPressed = buttonPressed; this.link = link; this.bar = pb;
    }

    GetExample(ListView listView, Activity activity, String buttonPressed, String link, String symbol){
        this.listView = listView; this.activity = activity; ;this.buttonPressed = buttonPressed; this.link = link; this.symbol = symbol;
    }

    GetExample(ListView listView, Activity activity, String buttonPressed, String link, String symbol, ProgressBar pb){
        this.listView = listView; this.activity = activity; ;this.buttonPressed = buttonPressed; this.link = link; this.symbol = symbol; this.bar = pb;
    }

    GetExample(ListView listView, Activity activity, String buttonPressed, String link, String symbol, JSONArray array){
        this.listView = listView; this.activity = activity; ;this.buttonPressed = buttonPressed; this.link = link; this.symbol = symbol; this.datastreams = array;
    }

    GetExample(String entity, String entityId){
        this.buttonPressed = "";
        this.entity = entity;
        this.entityId = entityId;

    }

    GetExample(String link) {
        this.getLink = link;
    }

    GetExample(){}

    String doGetRequest(String url) throws IOException {
        Request request = new Request.Builder()
                .url(url)
                .build();

        Response response = client.newCall(request).execute();
        return response.body().string();
    }

    public JSONArray getSensors() {
        return this.sensors;
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected String doInBackground(String... params) {

        if (buttonPressed.equals("things"))
            handleThingsBackground();
        else if (buttonPressed.equals("sensors"))
            handleSensorsBackground();
        else if (buttonPressed.equals("datastreams"))
            handleDatastreamsBackground();
        else if (buttonPressed.equals("observations"))
            handleObservationsBackground();
        else if (buttonPressed.equals("sensorThing"))
            handleSensorThingBackground();
        else if (buttonPressed.equals("observationList"))
            handleObservationsBackground();
        else if (buttonPressed.equals("datastreamAdd"))
            handleAddDatastreamBackground();
        else if (buttonPressed.equals("locations"))
            handleLocationsBackground();
        else if (entity.equals("Datastreams") | entity.equals("Things") ){
            String result = getDataById();
            return result;
        }
        else if (!getLink.equals("")){
            String result = getDataByLink();
            return result;
        }

        return null;
    }

    private String getDataByLink() {

        try {

            this.returnValue = doGetRequest( this.getLink );

        } catch (IOException e) {
            e.printStackTrace();
        }
        return this.returnValue;
    }

    private void handleLocationsBackground() {
        try {
            this.returnValue = doGetRequest("https://academic-hr-ad60.sensorup.com/v1.0/Locations");

            JObject = new JSONObject(this.returnValue);
            String count = JObject.getString("@iot.count");

            this.locationsList = JObject.getJSONArray("value");

            this.lst  = new ArrayList<String>(Integer.parseInt(count));

        } catch (IOException | JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onPreExecute() {
        if (this.buttonPressed.equals("things") || this.buttonPressed.equals("sensors")
                || this.buttonPressed.equals("sensorThing") || this.buttonPressed.equals("locations")
                || this.buttonPressed.equals("datastreamAdd") || this.buttonPressed.equals("observationList") | this.buttonPressed.equals("datastreams")) {
            if (this.bar == null)
                this.bar = (ProgressBar) this.activity.findViewById(R.id.progressBar);
            this.bar.setVisibility(View.VISIBLE);
        }
    }

    @Override
    protected void onProgressUpdate(Void... values) {}

    @Override
    protected void onPostExecute(String result) {
        if (buttonPressed.equals("things"))
            handleThingsPost();
        else if (buttonPressed.equals("sensors"))
            handleSensorsPost();
        else if (buttonPressed.equals("datastreams"))
            handleDatastreamsPost();
        else if (buttonPressed.equals("observations"))
            handleObservationsPost();
        else if (buttonPressed.equals("sensorThing"))
            handleSensorThingPost();
        else if (buttonPressed.equals("observationList"))
            handleObservationsPost();
        else if (buttonPressed.equals("datastreamAdd"))
            handleAddDatastreamPost();
        else if (buttonPressed.equals("locations"))
            handleLocationsPost();
        /*else if (entity.equals("datastreams")){
            getObservationData();
        }*/
    }


    /*private void getObservationData() {
        //this.txtView.setText(this.returnValue);
        Log.d("UDP", "Podaci o datastreamu dohvaceni sa get");
    }*/


    private String getDataById() {
        try {

            this.returnValue = doGetRequest("https://academic-hr-ad60.sensorup.com/v1.0/" + this.entity + "(" + this.entityId + ")" );

        } catch (IOException e) {
            e.printStackTrace();
        }
        return this.returnValue;
    }

    private void handleLocationsPost() {
        for (int i = 0; i < this.locationsList.length(); i++) {
            JSONObject object = null;

            try {
                object = this.locationsList.getJSONObject(i);
                String name = object.getString("name");
                lst.add(name);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        final ArrayAdapter<String> adapter;
        adapter = new ArrayAdapter<String>(this.activity, android.R.layout.simple_list_item_1, lst);

        this.listView.setAdapter(adapter);
        this.listView.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                try {
                    JSONObject object = locationsList.getJSONObject(position);

                    Intent intent = new Intent(activity, DisplayLocationTab.class);
                    intent.putExtra("location", object.toString());
                    activity.startActivity(intent);

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });

        this.bar.setVisibility(View.GONE);

    }

    private void handleThingsBackground () {
        try {
            this.returnValue = doGetRequest("https://academic-hr-ad60.sensorup.com/v1.0/Things?$expand=Locations");

            JObject = new JSONObject(this.returnValue);
            String count = JObject.getString("@iot.count");

            this.things = JObject.getJSONArray("value");

            String returnValueLocation = doGetRequest("https://academic-hr-ad60.sensorup.com/v1.0/Locations");
            this.locations = new JSONObject(returnValueLocation);
            //this.locations = locationObject.getJSONArray("value");

            this.lst  = new ArrayList<String>(Integer.parseInt(count));

        } catch (IOException | JSONException e) {
            e.printStackTrace();
        }
    }

    private void handleThingsPost () {

        List<Map<String, String>> data = new ArrayList<Map<String, String>>();
        //nul pzt
        for (int i=0; i<this.things.length(); i++) {

            JSONObject object = null;
            String thingName = "";

            try {
                object = this.things.getJSONObject(i);
                thingName = object.getString("name");
            } catch (JSONException e) {
                e.printStackTrace();
            }

            Map<String, String> datum = new HashMap<String, String>(2);
            datum.put("title", "Device name: ");
            datum.put("subtitle", thingName);
            data.add(datum);
        }
        SimpleAdapter adapter = new SimpleAdapter(this.activity, data,
                android.R.layout.simple_list_item_2,
                new String[] {"title", "subtitle"},
                new int[] {android.R.id.text1,
                        android.R.id.text2});
        this.listView.setAdapter(adapter);

        this.listView.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                try {
                    JSONObject object = things.getJSONObject(position);

                    Intent intent = new Intent(activity, DisplayThingTabActivity.class);
                    intent.putExtra("things", object.toString());
                    intent.putExtra("locations", locations.toString());

                    activity.startActivity(intent);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });

        this.bar.setVisibility(View.GONE);

    }

    private void handleSensorsBackground () {
        try {
            this.returnValue = doGetRequest("https://academic-hr-ad60.sensorup.com/v1.0/Sensors");
            JObject = new JSONObject(this.returnValue);
            String count = JObject.getString("@iot.count");

            this.sensors = JObject.getJSONArray("value");
            this.lst  = new ArrayList<String>(Integer.parseInt(count));

        } catch (IOException | JSONException e) {
            e.printStackTrace();
        }
    }

    private void handleSensorsPost () {

        for (int i = 0; i < sensors.length(); i++) {
            JSONObject object = null;

            try {
                object = sensors.getJSONObject(i);
                String name = object.getString("name");
                String description = object.getString("description");
                lst.add(name + " - " + description);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        /*final ArrayAdapter<String> adapter;
        adapter = new ArrayAdapter<String>(this.activity, android.R.layout.simple_list_item_1, lst);
        listView.setAdapter(adapter);*/

        List<Map<String, String>> data = new ArrayList<Map<String, String>>();
        for (int i=0; i<lst.size(); i++) {
            String[] sensorNameDesc = lst.get(i).split(" - ");

            Map<String, String> datum = new HashMap<String, String>(2);
            datum.put("title", "Name: " + sensorNameDesc[0]);
            datum.put("subtitle", "Description: " + sensorNameDesc[1]);
            data.add(datum);
        }
        SimpleAdapter adapter = new SimpleAdapter(this.activity, data,
                android.R.layout.simple_list_item_2,
                new String[] {"title", "subtitle"},
                new int[] {android.R.id.text1,
                        android.R.id.text2});
        this.listView.setAdapter(adapter);

        listView.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                try {
                    JSONObject object = sensors.getJSONObject(position);

                    Log.d("GetExample", "sensorObject " + object.toString());

                    Intent intent = new Intent(activity, DisplaySensorThing.class);
                    intent.putExtra("sensor", object.toString());

                    activity.startActivity(intent);


                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

        this.bar.setVisibility(View.GONE);

    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void handleSensorThingBackground () {
        try {

            this.returnValue = doGetRequest(this.link);
            JSONObject sensor = new JSONObject(this.returnValue);

            String datastreamsLink = sensor.getString("Datastreams@iot.navigationLink");
            String datastreamsReturn = doGetRequest(datastreamsLink);
            JSONObject datastreams = new JSONObject(datastreamsReturn);
            String count = datastreams.getString("@iot.count");
            datastreamsArray = datastreams.getJSONArray("value");

            lst = new ArrayList<String>(Integer.parseInt(count));

            sensorList = new ArrayList<JSONObject>(Integer.parseInt(count));
            thingList = new ArrayList<JSONObject>(Integer.parseInt(count));
            datastreamList = new ArrayList<JSONArray>(Integer.parseInt(count));
            datastreamListThing = new JSONObject();
            
            ArrayList<String> lstId = new ArrayList<String>(Integer.parseInt(count));
            for (int i = 0; i < Integer.parseInt(count); i++) {
                JSONObject datastream = (JSONObject) datastreamsArray.get(i);

                String thing = doGetRequest(datastream.getString("Thing@iot.navigationLink"));
                JSONObject thingObj = new JSONObject(thing);
                String thingObjId = thingObj.getString("@iot.id");

                String sensorString = doGetRequest(datastream.getString("Sensor@iot.navigationLink"));
                JSONObject sensorObj = new JSONObject(sensorString);
                String sensorObjId = sensorObj.getString("@iot.id");

                if (datastreamListThing.has(thingObj.getString("@iot.id"))) {
                    JSONObject datastreamsObj = datastreamListThing.getJSONObject(thingObjId);

                    JSONArray sensorDatastreamArray;
                    if (datastreamsObj.has(sensorObjId)) {
                        sensorDatastreamArray = datastreamsObj.getJSONArray(sensorObjId);
                        sensorDatastreamArray.put(datastream);
                    } else {
                        sensorDatastreamArray = new JSONArray();
                        sensorDatastreamArray.put(datastream);
                    }
                    datastreamsObj.put(sensorObjId, sensorDatastreamArray);

                    datastreamListThing.put(thingObjId, datastreamsObj);
                } else {
                    JSONObject datastreamsObj = new JSONObject();
                    JSONArray sensorDatastreamArray = new JSONArray();

                    sensorDatastreamArray.put(datastream);
                    datastreamsObj.put(sensorObjId, sensorDatastreamArray);
                    datastreamListThing.put(thingObjId, datastreamsObj);
                }

                if (!lstId.contains(sensorObj.getString("@iot.id") + " - " + thingObj.getString("@iot.id"))) {
                    lst.add(sensorObj.getString("name") + " - " + thingObj.getString("name"));
                    lstId.add(sensorObj.getString("@iot.id") + " - " + thingObj.getString("@iot.id"));
                    sensorList.add(sensorObj);
                    thingList.add(thingObj);
                    //datastream.put("severalDatasreams", false);
                    JSONArray datastreamArray = new JSONArray();
                    datastreamArray.put(datastream.toString());
                    datastreamList.add(datastreamArray);
                } else {
                    //datastream.put("severalDatasreams", true);
                    int index = lstId.indexOf(sensorObj.getString("@iot.id") + " - " + thingObj.getString("@iot.id"));
                    JSONArray datastreamArray = datastreamList.get(index);
                    datastreamArray.put(datastream.toString());
                    datastreamList.add(index, datastreamArray);
                }
            }

        } catch (IOException | JSONException e) {
            e.printStackTrace();
        }
    }

    private void handleSensorThingPost () {

        /*final ArrayAdapter<String> adapter;
        adapter = new ArrayAdapter<String>(this.activity, android.R.layout.simple_list_item_1, lst);

        listView.setAdapter(adapter);*/

        List<Map<String, String>> data = new ArrayList<Map<String, String>>();
        for (int i=0; i<lst.size(); i++) {
            String[] sensorThing = lst.get(i).split(" - ");

            Map<String, String> datum = new HashMap<String, String>(2);
            datum.put("title", "Sensor name: " + sensorThing[0]);
            datum.put("subtitle", "Device name: " + sensorThing[1]);
            data.add(datum);
        }
        SimpleAdapter adapter = new SimpleAdapter(this.activity, data,
                android.R.layout.simple_list_item_2,
                new String[] {"title", "subtitle"},
                new int[] {android.R.id.text1,
                        android.R.id.text2});
        this.listView.setAdapter(adapter);

        listView.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                try {
                    JSONObject datastreamObject = datastreamsArray.getJSONObject(position);
                    JSONObject sensorObject = sensorList.get(position);
                    JSONObject thingObject = thingList.get(position);
                    //JSONArray datastreamsArray = datastreamListThing.getJSONArray(thingObject.getString("@iot.id"));
                    //JSONArray datastreamsArray = datastreamList.get(position);

                    JSONObject datastreamThing = datastreamListThing.getJSONObject(thingObject.getString("@iot.id"));
                    JSONArray datastreamsArray = datastreamThing.getJSONArray(sensorObject.getString("@iot.id"));

                    Intent intent = new Intent(activity, DisplaySensorTabActivity.class);
                    intent.putExtra("datastream", datastreamsArray.toString());
                    intent.putExtra("sensor", sensorObject.toString());
                    intent.putExtra("thing", thingObject.toString());

                    activity.startActivity(intent);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

        this.bar.setVisibility(View.GONE);

    }


    private void handleDatastreamsPost () {

        for (int i = 0; i < datastreams.length(); i++) {
            JSONObject object = null;

            try {
                object = datastreams.getJSONObject(i);
                //String name = "Datastream - " + object.getString("name");
                String name = object.getString("name");

                lst.add(name);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        final ArrayAdapter<String> adapter;
        adapter = new ArrayAdapter<String>(this.activity, android.R.layout.simple_list_item_1, lst);


        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                try {
                    JSONObject object = datastreams.getJSONObject(position);
                    JSONObject unit = object.getJSONObject("unitOfMeasurement");

                    symbol = unit.getString("symbol");

                    final String observationLink = object.getString("Observations@iot.navigationLink");

//                    String returnValue = doGetRequest(object.getString("Sensor@iot.navigationLink"));
//                    JObject = new JSONObject(returnValue);
                    Request request = new Request.Builder()
                            //.header("Authorization", "token abcd")
                            .url(observationLink)
                            .build();

                    client.newCall(request).enqueue(new Callback() {
                        @Override
                        public void onFailure(Call call, IOException e) {
                            e.printStackTrace();
                        }

                        @Override
                        public void onResponse(Call call, final Response response) throws IOException {

                            try {
                                String observationsString = response.body().string();
                                JSONObject observationsObj = new JSONObject(observationsString);
                                observations = observationsObj.getJSONArray("value");
                                activity.runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        //stuff that updates ui
                                        handleObservationsPost();
                                    }
                                });



                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            if (!response.isSuccessful()) {
                                throw new IOException("Unexpected code " + response);
                            }
                        }
                    });

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

        if (this.bar != null)
            this.bar.setVisibility(View.GONE);
    }

    private void handleDatastreamsBackground () {
        try {
            this.returnValue = doGetRequest(this.link);
            JObject = new JSONObject(this.returnValue);
            String count = JObject.getString("@iot.count");

            this.datastreams = JObject.getJSONArray("value");
            this.lst  = new ArrayList<String>(Integer.parseInt(count));

        } catch (IOException | JSONException e) {
            e.printStackTrace();
        }
    }

    private void handleObservationsBackground() {
        try {
            this.returnValue = doGetRequest(this.link);

            JObject = new JSONObject(this.returnValue);
            String count = JObject.getString("@iot.count");

            this.observations = JObject.getJSONArray("value");
            this.lst  = new ArrayList<String>(Integer.parseInt(count));

            if (JObject.has("unitOfMeasurement") ) {

                observationSymbol =  JObject.getJSONObject("unitOfMeasurement").getString("symbol");
            }
            else observationSymbol = "";

        } catch (IOException | JSONException e) {
            e.printStackTrace();
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    private void handleObservationsPost () {

        lst.clear();
        listView.setAdapter(null);

        if (this.activity.findViewById(R.id.textViewNoObservations) != null)
        {
            TextView tv = (TextView) this.activity.findViewById(R.id.textViewNoObservations);
            tv.setVisibility(View.VISIBLE);
        }

        for (int i = 0; i < this.observations.length(); i++) {
            JSONObject object = null;

            try {

                object = this.observations.getJSONObject(i);

                String phenomenonTime = object.getString("phenomenonTime");
                phenomenonTime = phenomenonTime.replace("T", " ");

                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                Date convertedDate=null;
                try {
                    convertedDate = dateFormat.parse(phenomenonTime);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                SimpleDateFormat fmtOut = new SimpleDateFormat("dd.MM.yyyy. HH:mm:ss z");
                phenomenonTime = fmtOut.format(convertedDate);

                if (this.symbol.equals("none"))
                    this.symbol = "";

                String name =  phenomenonTime + " - " + object.getString("result") + " " + this.symbol;
                lst.add(name);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        if (this.datastreams != null) {

            final Button backButton = (Button) this.activity.findViewById(R.id.backButtonFromObservations);
            backButton.setVisibility(View.VISIBLE);

            backButton.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    lst.clear();
                    listView.setAdapter(null);
                    if (activity.findViewById(R.id.textViewNoObservations) != null)
                    {
                        TextView tv = (TextView) activity.findViewById(R.id.textViewNoObservations);
                        tv.setVisibility(View.GONE);
                    }
                    handleDatastreamsPost();
                    backButton.setVisibility(View.GONE);
                }
            });
        }

        /*final ArrayAdapter<String> adapter;
        adapter = new ArrayAdapter<String>(this.activity, android.R.layout.simple_list_item_1, lst);

        this.listView.setAdapter(adapter);*/

        List<Map<String, String>> data = new ArrayList<Map<String, String>>();

        if (this.lst.size() > 0) {
            if (activity.findViewById(R.id.textViewNoObservations) != null) {
                TextView tv = (TextView) activity.findViewById(R.id.textViewNoObservations);
                tv.setVisibility(View.GONE);
            }
        }

        for (int i=0; i<lst.size(); i++) {
            String[] timeResult = lst.get(i).split(" - ");

            Map<String, String> datum = new HashMap<String, String>(2);
            datum.put("title", "Result: " + timeResult[1]);
            datum.put("subtitle", "Date and time: " + timeResult[0]);
            data.add(datum);
        }
        SimpleAdapter adapter = new SimpleAdapter(this.activity, data,
                android.R.layout.simple_list_item_2,
                new String[] {"title", "subtitle"},
                new int[] {android.R.id.text1,
                        android.R.id.text2});
        this.listView.setAdapter(adapter);

        this.listView.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                try {
                    JSONObject object = observations.getJSONObject(position);

                    Intent intent = new Intent(activity, DisplayObservationInfoActivity.class);
                    intent.putExtra("observation", object.toString());

                    activity.startActivity(intent);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });

        if (bar != null)
            bar.setVisibility(View.GONE);
    }


    private JSONObject sensorsObject, thingsObject;
    private void handleAddDatastreamBackground() {
        try {
            String sensors = doGetRequest("https://academic-hr-ad60.sensorup.com/v1.0/Sensors");
            sensorsObject = new JSONObject(sensors);

            String things = doGetRequest("https://academic-hr-ad60.sensorup.com/v1.0/Things");
            thingsObject = new JSONObject(things);

        } catch (IOException | JSONException e) {
            e.printStackTrace();
        }
    }

    private void handleAddDatastreamPost () {

        final JSONObject datastreamToPost = new JSONObject();


        final String[] thingId = new String[1];
        final String[] sensorId = new String[1];
        final String[] sensorType = {""};

        Spinner dynamicSpinnerThings = (Spinner) this.activity.findViewById(R.id.thingNameSpinner);

        int count = 0;
        String[] thingNames = new String[0];
        JSONArray thingValue = null;
        try {
            thingValue = this.thingsObject.getJSONArray("value");
            count = Integer.parseInt(this.thingsObject.getString("@iot.count"));
            thingNames = new String[count];
            for (int i = 0; i < count; i++) {
                JSONObject currentThing = (JSONObject) thingValue.get(i);
                thingNames[i] = currentThing.getString("name");
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this.activity,
                android.R.layout.simple_spinner_item, thingNames);

        final JSONArray allThings = thingValue;
        dynamicSpinnerThings.setAdapter(adapter);
        dynamicSpinnerThings.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                Log.v("item", (String) parent.getItemAtPosition(position));
                try {
                    JSONObject selectedThing = (JSONObject) allThings.get(position);
                    thingId[0] = selectedThing.getString("@iot.id");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub
            }
        });

        Spinner dynamicSpinnerSensors = (Spinner) this.activity.findViewById(R.id.sensorTypeSpinner);

        count = 0;
        JSONArray sensorValue = null;
        String[] sensorNames = new String[0];
        try {

            count = Integer.parseInt(this.sensorsObject.getString("@iot.count"));
            sensorNames = new String[count];
            sensorValue = this.sensorsObject.getJSONArray("value");
            for (int i = 0; i < count; i++) {
                JSONObject currentSensor = (JSONObject) sensorValue.get(i);
                sensorNames[i] = currentSensor.getString("name");
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        ArrayAdapter<String> adapter2 = new ArrayAdapter<String>(this.activity,
                android.R.layout.simple_spinner_item, sensorNames);

        final JSONArray allTSensors = sensorValue;
        dynamicSpinnerSensors.setAdapter(adapter2);
        dynamicSpinnerSensors.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                Log.v("item", (String) parent.getItemAtPosition(position));
                try {
                    JSONObject selectedSensor = (JSONObject) allTSensors.get(position);
                    sensorId[0] = selectedSensor.getString("@iot.id");

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub
            }
        });

        Button addSensor = (Button) this.activity.findViewById(buttonAddSensor);

        addSensor.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                EditText name = (EditText) activity.findViewById(textDatastreamIotNameNewChange);
                EditText description = (EditText) activity.findViewById(textdatastreamIotDescriptionNewChange);

                String nameS = name.getText().toString();
                String descriptionS = description.getText().toString();

                try {
                    if (nameS.isEmpty() || descriptionS.isEmpty()) {
                        CharSequence text = "Please enter name and description,";
                        int duration = Toast.LENGTH_SHORT;

                        Toast toast = Toast.makeText(activity, text, duration);
                        toast.show();
                    } else {
                        datastreamToPost.put("name", nameS);
                        datastreamToPost.put("description", descriptionS);
                        datastreamToPost.put("observationType", "http://www.opengis.net/def/observationType/OGC-OM/2.0/OM_Measurement");
                        JSONObject thing = new JSONObject();
                        thing.put("@iot.id", thingId[0]);
                        datastreamToPost.put("Thing", thing);
                        JSONObject sensor = new JSONObject();
                        sensor.put("@iot.id", sensorId[0]);
                        datastreamToPost.put("Sensor", sensor);

                        PostExample post = new PostExample(activity, datastreamToPost, "datastream");
                        post.execute();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

        this.bar.setVisibility(View.GONE);
    }
}
