package com.example.monika.sensornetwork;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Monika on 26.5.2017..
 */

public class SensorTab3Observations extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.snesor3tabobservation, container, false);

        ProgressBar progressBar = (ProgressBar) rootView.findViewById(R.id.progressBar);

        Bundle bundle=getArguments();
        if(bundle!=null) {

            if (bundle.containsKey("datastreams")) {

                JSONArray datastreamsArray = null;
                try {
                    datastreamsArray = new JSONArray(bundle.getString("datastreams"));

                    final ListView lv = (ListView) rootView.findViewById(R.id.sensorsDatastreamView);
                    final Activity activity = getActivity();

                    if (datastreamsArray.length() == 1) {

                        JSONObject datastream = new JSONObject(datastreamsArray.getString(0));

                        String observationLink = datastream.getString("Observations@iot.navigationLink");
                        String observationSymbol =  datastream.getJSONObject("unitOfMeasurement").getString("symbol");

                        GetExample example = new GetExample(lv, activity, "observationList", observationLink, observationSymbol, progressBar);
                        example.execute();

                    } else if ( datastreamsArray.length() > 1 ) {

                        final ArrayList<String> lst = new ArrayList<String>(datastreamsArray.length());
                        for (int i = 0; i < datastreamsArray.length(); i++) {
                            JSONObject object = null;
                            try {
                                object = datastreamsArray.getJSONObject(i);
                                String name = object.getString("name");
                                lst.add(name);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        final ArrayAdapter<String> adapter;
                        adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, lst);
                        lv.setAdapter(adapter);

                        final JSONArray finalDatastreamsArray = datastreamsArray;
                        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                try {
                                    JSONObject object = finalDatastreamsArray.getJSONObject(position);
                                    String link = object.getString("Observations@iot.navigationLink");
                                    String symbol = object.getJSONObject("unitOfMeasurement").getString("symbol");
                                    GetExample example = new GetExample(lv, getActivity(), "observations", link, symbol, finalDatastreamsArray);
                                    example.execute();
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
        return rootView;
    }
}
