package com.example.monika.sensornetwork;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ListView;

import java.io.IOException;

public class DisplayThingsActivity extends AppCompatActivity {

    public void loadThings (View view) throws IOException {

        ListView lv = (ListView) findViewById(R.id.thingsView);

        GetExample example = new GetExample(lv, this, "things");
        example.execute();

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display_things);

        ListView lv = (ListView) findViewById(R.id.thingsView);

        GetExample example = new GetExample(lv, this, "things");
        example.execute();

    }
}