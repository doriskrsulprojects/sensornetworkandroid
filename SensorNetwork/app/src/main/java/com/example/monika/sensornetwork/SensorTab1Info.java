package com.example.monika.sensornetwork;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Monika on 26.5.2017..
 */

public class SensorTab1Info extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.sensor1tabinfo, container, false);

        Bundle bundle=getArguments();
        if(bundle!=null) {

            if (bundle.containsKey("sensor")) {
                try {
                    JSONObject jsonObj = new JSONObject(bundle.getString("sensor"));
                    JSONArray datastreamObj = new JSONArray(bundle.getString("datastreams"));
                    JSONObject thingObj = new JSONObject(bundle.getString("thing"));
                    Log.d("DisplaySensorTabThing-2", thingObj.toString());


                    TextView iotIdValue = (TextView) rootView.findViewById(R.id.textSensorIotIdValue);
                    TextView iotSelfLinkValue = (TextView) rootView.findViewById(R.id.textSensorIotSelfLinkValue);
                    TextView iotNameValue = (TextView) rootView.findViewById(R.id.textSensorIotNameValue);
                    TextView iotDescriptionValue = (TextView) rootView.findViewById(R.id.textSensorDescriptionValue);
                    TextView iotMetadataValue = (TextView) rootView.findViewById(R.id.textSensorMetadataValue);
                    TextView iotDatastreamsNavLink = (TextView) rootView.findViewById(R.id.textSensorDatastreamsLinkValue);

                    iotIdValue.setText(String.valueOf(jsonObj.getInt("@iot.id")));

                    String selfLink = jsonObj.getString("@iot.selfLink");
                    selfLink = selfLink.substring(0, 38) + "display/" + selfLink.substring(38, selfLink.length());
                    iotSelfLinkValue.setText(selfLink);

                    iotNameValue.setText(jsonObj.getString("name"));
                    iotDescriptionValue.setText(jsonObj.getString("description"));
                    iotMetadataValue.setText(jsonObj.getString("metadata"));

                    String selfLinkDatastream = jsonObj.getString("Datastreams@iot.navigationLink");
                    selfLinkDatastream = selfLinkDatastream.substring(0, 38) + "display/" + selfLinkDatastream.substring(38, selfLinkDatastream.length());
                    iotDatastreamsNavLink.setText(selfLinkDatastream);

                    TextView iotThingIdValue = (TextView) rootView.findViewById(R.id.textSensorThingIdValue);
                    TextView iotThingNameValue = (TextView) rootView.findViewById(R.id.textSensorThingNameValue);

                    iotThingIdValue.setText(String.valueOf(thingObj.getInt("@iot.id")));
                    iotThingNameValue.setText(thingObj.getString("name"));


                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }

        return rootView;
    }
}