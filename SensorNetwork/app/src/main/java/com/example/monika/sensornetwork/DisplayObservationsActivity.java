package com.example.monika.sensornetwork;

import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.widget.LinearLayout;
import android.widget.ListView;

import org.json.JSONException;
import org.json.JSONObject;

public class DisplayObservationsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_observations);


        Bundle bundle = new Bundle();

        Bundle extras = getIntent().getExtras();

        String datastreamObservationsLink = extras.getString("datastreamObservationsLink");
        String symbol = extras.getString("unitOfMeasurement.symbol");

        ListView lv = (ListView) findViewById(R.id.observationsListView);

        GetExample example = new GetExample(lv, this, "observationList", datastreamObservationsLink, symbol);
        example.execute();

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

    }
}
