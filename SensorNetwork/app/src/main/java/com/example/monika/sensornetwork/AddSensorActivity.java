package com.example.monika.sensornetwork;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ProgressBar;

public class AddSensorActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_sensor);
        ProgressBar pb = (ProgressBar) findViewById(R.id.progressBarSensors);
        GetExample example = new GetExample(this, "datastreamAdd", pb);
        example.execute();
    }
}
