package com.example.monika.sensornetwork;

import android.content.Context;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * Created by Matija on 6/6/2017.
 */

public class CallNotificationIntent {

    CallNotificationIntent(JSONArray sensorData, Context context){

        MyNotification notification = new MyNotification(sensorData, context);
        notification.execute();

    }
}
